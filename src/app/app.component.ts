import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-new-angular-app';

  async connectSerial() {
    alert('sudip');
    try {
      
      const port = await (navigator as any).serial.requestPort({
        /* filters: [
            {
              vendorId: 0x0403, // FTDI
              productId: 0x6001,
            },
          ],*/
      });
      console.log("port", port);
      console.log("port info", port.getInfo());

      await port.open({
        baudRate: 9600,
        dataBits: 8,
        stopBits: 1,
        parity: "none"
      });
      const reader = port.readable.getReader();

      // Listen to data coming from the serial device.
      while (true) {
        const { value, done } = await reader.read();
        if (done) {
          // Allow the serial port to be closed later.
          reader.releaseLock();
          break;
        }
        var string = new TextDecoder().decode(value);
        // value is a Uint8Array.
        console.log(string);
      }
    //   const textDecoder = new TextDecoderStream();
    //   const readableStreamClosed = port.readable.pipeTo(textDecoder.writable);
    //   const reader = textDecoder.readable.getReader();
    //   let i = 0;

    //   while (true) {
    //     const { value, done } = await reader.read();
    //     i++;
    //     if (done) {
    //       // |reader| has been canceled.
    //       console.log("stop", done);
    //       break;
    //     }
    //     if (value) {
    //       // Do something with |value|...
    //     }
    //   }

    //   reader.releaseLock();
    } catch (error) {
      console.log(error);
    }
  }
  
}

export class serialPortTest {
  
  async connectSerial() {
    alert('sudip');
    try {
      
      const port = await (navigator as any).serial.requestPort({
        /* filters: [
            {
              vendorId: 0x0403, // FTDI
              productId: 0x6001,
            },
          ],*/
      });
      console.log("port", port);
      console.log("port info", port.getInfo());

      await port.open({
        baudRate: 9600,
        dataBits: 8,
        stopBits: 1,
        parity: "none"
      });
      const reader = port.readable.getReader();

      // Listen to data coming from the serial device.
      while (true) {
        const { value, done } = await reader.read();
        if (done) {
          // Allow the serial port to be closed later.
          reader.releaseLock();
          break;
        }
        var string = new TextDecoder().decode(value);
        // value is a Uint8Array.
        console.log(string);
      }
    //   const textDecoder = new TextDecoderStream();
    //   const readableStreamClosed = port.readable.pipeTo(textDecoder.writable);
    //   const reader = textDecoder.readable.getReader();
    //   let i = 0;

    //   while (true) {
    //     const { value, done } = await reader.read();
    //     i++;
    //     if (done) {
    //       // |reader| has been canceled.
    //       console.log("stop", done);
    //       break;
    //     }
    //     if (value) {
    //       // Do something with |value|...
    //     }
    //   }

    //   reader.releaseLock();
    } catch (error) {
      console.log(error);
    }
  }
}
